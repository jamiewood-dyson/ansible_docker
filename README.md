# Example

Examples based on csi infra repo

## Build

`docker build -t "ansible-docker" .`

## Run

* `cd infrastructure/ansible`
* `docker run --rm -it -e VAULT_ADDR=http://vault.mgt.digital.dyson.cloud:8200 -e VAULT_TOKEN=$REDACTED$ -v $(pwd):/ansible/playbooks ansible-docker ansible-playbook --vault-id ./vault_key.py -i inventory/sitecore9_hosts playbooks/sitecore9.yml --limit qa01 --check`

Alternatively, the image has been pushed into the tools account ecr

* `$(aws ecr get-login --no-include-email --profule tools-eu --region eu-west-1)`
* `docker run --rm -it -e VAULT_ADDR=http://vault.mgt.digital.dyson.cloud:8200 -e VAULT_TOKEN=$REDACTED$ -v $(pwd):/ansible/playbooks 622156871794.dkr.ecr.eu-west-1.amazonaws.com/ansible_docker:latest ansible-playbook --vault-id ./vault_key.py -i inventory/sitecore9_hosts playbooks/sitecore9.yml --limit qa01 --check`

## Tags

Currently the `latest`/`2_8_2` and `2_8_1` tags have been pushed.


### Notes

https://eu-west-1.console.aws.amazon.com/ecr/repositories/ansible_docker/?region=eu-west-1


## Todo

Bitbucket Pipeline to build image and push to ECR

